using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround_parallax : MonoBehaviour
{
    [SerializeField] GameObject houseSprite = null, floorSprite = null, skySprite = null,backgroundBlackSprite = null,treesSprite = null;
    public GameObject playerSPrite = null;

    [SerializeField] float houseSpeed = 0f, skySpeed = 0f, GroundSpeed = 0f, backgroundSpeed = 0f, treesSpeed = 0f;

    private void Awake()
    {
        if (houseSprite == null || floorSprite == null || skySprite == null|| treesSprite ==null)
            print("something is null");
        if (backgroundBlackSprite == null)
            print("BG list is null");
        if (playerSPrite == null)
            print("PLAYER is " + playerSPrite);
    }

    private void Update()
    {
        
    }

    private void LateUpdate()
    {
        MoveAllTheElements(Player.Direction);
    }
    #region Parallax
    void MoveAllTheElements(int Dir)
    {
        backgroundBlackSprite.transform.position += new Vector3(-Dir * backgroundSpeed*Time.deltaTime, 0f);
        houseSprite.transform.position += new Vector3(-Dir * houseSpeed * Time.deltaTime, 0f);
        floorSprite.transform.position += new Vector3(-Dir * GroundSpeed * Time.deltaTime, 0f);
        skySprite.transform.position += new Vector3(-Dir * skySpeed * Time.deltaTime, 0f);
        treesSprite.transform.position += new Vector3(-Dir * treesSpeed * Time.deltaTime, 0f);
    }

    void SpritesManager()
    {
        
    }
    #endregion
}
