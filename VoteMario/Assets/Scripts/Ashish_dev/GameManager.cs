using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager instance_ = null;
    private Text healthText;

    //so in other scripts just use GameManger.instance.(anything)

    public bool GameOver = false,GamePaused = false;
    public int Lives = 5;

    private void Awake()
    {
        if (instance_ == null)
        {
            instance_ = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if(instance_== this)
        {
            Destroy(this.gameObject);
        }
        Application.targetFrameRate = 60;       // this will keep the frame rate constant
        healthText = GameObject.Find("healthText").GetComponent<Text>();
    }
    public void DisplayHealth(int health)
    {
        healthText.text = "Health: " + health;
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        
    }

    //write your methods below
}
