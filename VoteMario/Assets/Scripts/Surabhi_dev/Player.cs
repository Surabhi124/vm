using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer sr;
    [SerializeField] float movementSpeed = 6f, jumpSpeed = 0f;
    public static int Direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //float horizontalInput = Input.GetAxis("Horizontal");
        //float verticalInput = Input.GetAxis("Vertical");

        //rb.velocity = new Vector3(horizontalInput * movementSpeed, rb.velocity.y, verticalInput * movementSpeed);
        PlayerDirectionChanger();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movementSpeed * Direction, rb.velocity.y);
    }
    void PlayerDirectionChanger()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Direction = -1;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            Direction = 1;
        }
        sr.flipX = Direction == -1 ? true : false;
    }
}
