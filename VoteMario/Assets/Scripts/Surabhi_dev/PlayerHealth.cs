using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{/*
    public int health = 100;
    public Text text;

    void Start()
    {
        GameplayController.instance.DisplayHealth(health);
    }

    public void ApplyDamage(int damageAmount)
    {

        health -= damageAmount;

        if (health < 0)
        {
            health = 0;
        }

        // DISPLAY THE HEALTH VALUE
        GameplayController.instance.DisplayHealth(health);

     
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Health: " + health;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            health = health - 10;
        }
    }*/

    public int health = 100;
    void Start()
    {
        GameManager.instance_.DisplayHealth(health);
    }
}
