using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    /*
     public GameObject obstacle;
     public float maxX;
     public float minX;
     public float maxY;
     public float minY;
     public float timespawn;
     private float spawntime;
     void Update()
     {
         if (Time.time > spawntime)
         {
             Spawn();
             spawntime = Time.time + timespawn;
         }
     }
     void Spawn()
     {
         float randomX = Random.Range(minX, maxX);
         float randomY = Random.Range(minY, maxY);
         Instantiate(obstacle, transform.position + new Vector3(randomX, randomY, 0), transform.rotation);
     }
 }
    /*
     public GameObject[] enemies;

     public Vector3 spawnValues;
     public float spawnWait;
     public int startWait;
     public float spawnMostWait;
     public float spawnLeastWait;
     public float spawnYvalue;
     public bool stop;

     int randEnemy;
     void Start()
     {

         StartCoroutine(waitSpawner());
     }

     // Update is called once per frame
     void Update()
     {
         spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
     }

     IEnumerator waitSpawner()
     {
         yield return new WaitForSeconds(startWait);

         while (!stop)
         {
             randEnemy = Random.Range(0, 2);

             Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),
                 spawnYvalue, Random.Range(-spawnValues.z, spawnValues.z));

             Instantiate(enemies[randEnemy], spawnPosition + transform.TransformPoint(0, 0, 0),
             transform.rotation = Quaternion.Euler(transform.rotation.x * 0 + 10, transform.rotation.y, transform.rotation.z));


             yield return new WaitForSeconds(spawnWait);

         }
     }*/
    public GameObject enemy;
    public int xPos;
    public int zPos;
    public int enemycount;
    private void Start()
    {
        StartCoroutine(EnemyDrop());
    }
    IEnumerator EnemyDrop()
    {
        while(enemycount < 3)
        {
            xPos = Random.Range(1, 50);
            zPos = Random.Range(1, 60);
            Instantiate(enemy, new Vector3(xPos, -2, zPos), Quaternion.identity);
            yield return new WaitForSeconds(10f);
            enemycount += 1;
        }
    }

}
